/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Package filter_impl is for being compatible with older dubbo-go, please use `imports` package.
// It may be DEPRECATED OR REMOVED in the future.
package filter_impl

import (
	_ "gitee.com/git4chen/dubbo-go/filter/accesslog"
	_ "gitee.com/git4chen/dubbo-go/filter/active"
	_ "gitee.com/git4chen/dubbo-go/filter/adaptivesvc"
	_ "gitee.com/git4chen/dubbo-go/filter/auth"
	_ "gitee.com/git4chen/dubbo-go/filter/echo"
	_ "gitee.com/git4chen/dubbo-go/filter/exec_limit"
	_ "gitee.com/git4chen/dubbo-go/filter/generic"
	_ "gitee.com/git4chen/dubbo-go/filter/graceful_shutdown"
	_ "gitee.com/git4chen/dubbo-go/filter/hystrix"
	_ "gitee.com/git4chen/dubbo-go/filter/metrics"
	_ "gitee.com/git4chen/dubbo-go/filter/polaris/limit"
	_ "gitee.com/git4chen/dubbo-go/filter/seata"
	_ "gitee.com/git4chen/dubbo-go/filter/sentinel"
	_ "gitee.com/git4chen/dubbo-go/filter/token"
	_ "gitee.com/git4chen/dubbo-go/filter/tps"
	_ "gitee.com/git4chen/dubbo-go/filter/tracing"
)
