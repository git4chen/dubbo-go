/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

// Package imports is a one-stop collection of Dubbo SPI implementations that aims to help users with plugin installation
// by leveraging Go package initialization.
package imports

import (
	_ "gitee.com/git4chen/dubbo-go/cluster/cluster/adaptivesvc"
	_ "gitee.com/git4chen/dubbo-go/cluster/cluster/available"
	_ "gitee.com/git4chen/dubbo-go/cluster/cluster/broadcast"
	_ "gitee.com/git4chen/dubbo-go/cluster/cluster/failback"
	_ "gitee.com/git4chen/dubbo-go/cluster/cluster/failfast"
	_ "gitee.com/git4chen/dubbo-go/cluster/cluster/failover"
	_ "gitee.com/git4chen/dubbo-go/cluster/cluster/failsafe"
	_ "gitee.com/git4chen/dubbo-go/cluster/cluster/forking"
	_ "gitee.com/git4chen/dubbo-go/cluster/cluster/zoneaware"
	_ "gitee.com/git4chen/dubbo-go/cluster/loadbalance/aliasmethod"
	_ "gitee.com/git4chen/dubbo-go/cluster/loadbalance/consistenthashing"
	_ "gitee.com/git4chen/dubbo-go/cluster/loadbalance/iwrr"
	_ "gitee.com/git4chen/dubbo-go/cluster/loadbalance/leastactive"
	_ "gitee.com/git4chen/dubbo-go/cluster/loadbalance/p2c"
	_ "gitee.com/git4chen/dubbo-go/cluster/loadbalance/random"
	_ "gitee.com/git4chen/dubbo-go/cluster/loadbalance/roundrobin"
	_ "gitee.com/git4chen/dubbo-go/cluster/router/condition"
	_ "gitee.com/git4chen/dubbo-go/cluster/router/meshrouter"
	_ "gitee.com/git4chen/dubbo-go/cluster/router/polaris"
	_ "gitee.com/git4chen/dubbo-go/cluster/router/script"
	_ "gitee.com/git4chen/dubbo-go/cluster/router/tag"
	_ "gitee.com/git4chen/dubbo-go/config_center/nacos"
	_ "gitee.com/git4chen/dubbo-go/config_center/zookeeper"
	_ "gitee.com/git4chen/dubbo-go/filter/accesslog"
	_ "gitee.com/git4chen/dubbo-go/filter/active"
	_ "gitee.com/git4chen/dubbo-go/filter/adaptivesvc"
	_ "gitee.com/git4chen/dubbo-go/filter/auth"
	_ "gitee.com/git4chen/dubbo-go/filter/echo"
	_ "gitee.com/git4chen/dubbo-go/filter/exec_limit"
	_ "gitee.com/git4chen/dubbo-go/filter/generic"
	_ "gitee.com/git4chen/dubbo-go/filter/graceful_shutdown"
	_ "gitee.com/git4chen/dubbo-go/filter/hystrix"
	_ "gitee.com/git4chen/dubbo-go/filter/metrics"
	_ "gitee.com/git4chen/dubbo-go/filter/otel/trace"
	_ "gitee.com/git4chen/dubbo-go/filter/polaris/limit"
	_ "gitee.com/git4chen/dubbo-go/filter/seata"
	_ "gitee.com/git4chen/dubbo-go/filter/sentinel"
	_ "gitee.com/git4chen/dubbo-go/filter/token"
	_ "gitee.com/git4chen/dubbo-go/filter/tps"
	_ "gitee.com/git4chen/dubbo-go/filter/tps/limiter"
	_ "gitee.com/git4chen/dubbo-go/filter/tps/strategy"
	_ "gitee.com/git4chen/dubbo-go/filter/tracing"
	_ "gitee.com/git4chen/dubbo-go/metadata/mapping/metadata"
	_ "gitee.com/git4chen/dubbo-go/metadata/report/etcd"
	_ "gitee.com/git4chen/dubbo-go/metadata/report/nacos"
	_ "gitee.com/git4chen/dubbo-go/metadata/report/zookeeper"
	_ "gitee.com/git4chen/dubbo-go/metadata/service/exporter/configurable"
	_ "gitee.com/git4chen/dubbo-go/metadata/service/local"
	_ "gitee.com/git4chen/dubbo-go/metadata/service/remote"
	_ "gitee.com/git4chen/dubbo-go/metrics/app_info"
	_ "gitee.com/git4chen/dubbo-go/metrics/prometheus"
	_ "gitee.com/git4chen/dubbo-go/otel/trace/jaeger"
	_ "gitee.com/git4chen/dubbo-go/otel/trace/otlp"
	_ "gitee.com/git4chen/dubbo-go/otel/trace/stdout"
	_ "gitee.com/git4chen/dubbo-go/otel/trace/zipkin"
	_ "gitee.com/git4chen/dubbo-go/protocol/dubbo"
	_ "gitee.com/git4chen/dubbo-go/protocol/jsonrpc"
	_ "gitee.com/git4chen/dubbo-go/protocol/rest"
	_ "gitee.com/git4chen/dubbo-go/protocol/triple"
	_ "gitee.com/git4chen/dubbo-go/protocol/triple/health"
	_ "gitee.com/git4chen/dubbo-go/protocol/triple/reflection"
	_ "gitee.com/git4chen/dubbo-go/proxy/proxy_factory"
	_ "gitee.com/git4chen/dubbo-go/registry/directory"
	_ "gitee.com/git4chen/dubbo-go/registry/etcdv3"
	_ "gitee.com/git4chen/dubbo-go/registry/nacos"
	_ "gitee.com/git4chen/dubbo-go/registry/polaris"
	_ "gitee.com/git4chen/dubbo-go/registry/protocol"
	_ "gitee.com/git4chen/dubbo-go/registry/servicediscovery"
	_ "gitee.com/git4chen/dubbo-go/registry/xds"
	_ "gitee.com/git4chen/dubbo-go/registry/zookeeper"
	_ "gitee.com/git4chen/dubbo-go/xds/client/controller/version/v2"
	_ "gitee.com/git4chen/dubbo-go/xds/client/controller/version/v3"
)
